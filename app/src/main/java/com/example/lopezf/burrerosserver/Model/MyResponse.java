package com.example.lopezf.burrerosserver.Model;

import java.util.List;

/**
 * Created by lopezf on 4/2/2018.
 */

public class MyResponse {
    public long multicast_id;
    public int success;
    public int failure;
    public int canonical_ids;
    public List<Result> results;

}
