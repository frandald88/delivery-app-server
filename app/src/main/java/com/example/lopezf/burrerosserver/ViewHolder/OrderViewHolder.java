package com.example.lopezf.burrerosserver.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.lopezf.burrerosserver.Interface.ItemClickListener;
import com.example.lopezf.burrerosserver.R;

/**
 * Created by lopezf on 3/23/2018.
 */

public class OrderViewHolder extends RecyclerView.ViewHolder {

    public TextView txtOrderid,txtOrderStatus,txtOrderPhone,txtOrderAddress,txtOrderDate;
    public Button btnEdit,btnRemove,btnDetail,btnDirection;

    public OrderViewHolder(View itemView) {
        super(itemView);

        txtOrderAddress = (TextView)itemView.findViewById(R.id.order_address);
        txtOrderid = (TextView)itemView.findViewById(R.id.order_id);
        txtOrderStatus = (TextView)itemView.findViewById(R.id.order_status);
        txtOrderPhone = (TextView)itemView.findViewById(R.id.order_phone);
        txtOrderDate = (TextView)itemView.findViewById(R.id.order_date);

        btnEdit = (Button)itemView.findViewById(R.id.btnEdit);
        btnRemove = (Button)itemView.findViewById(R.id.btnRemove);
        btnDetail = (Button)itemView.findViewById(R.id.btnDetail);
        btnDirection = (Button)itemView.findViewById(R.id.btnDirection);
    }

}
