package com.example.lopezf.burrerosserver.Interface;

import android.view.View;

/**
 * Created by lopezf on 3/24/2018.
 */

public interface ItemClickListener {
    void onClick (View view, int position, boolean isLongClick);
}
