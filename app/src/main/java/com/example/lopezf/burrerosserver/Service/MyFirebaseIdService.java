package com.example.lopezf.burrerosserver.Service;

import com.example.lopezf.burrerosserver.Common.Common;
import com.example.lopezf.burrerosserver.Model.Token;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by lopezf on 4/2/2018.
 */

public class MyFirebaseIdService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        if (Common.currentUser != null)
        updateToServer(refreshedToken);


    }

    private void updateToServer(String refreshedToken) {
        //copy code from client app
        if (Common.currentUser != null) {
            FirebaseDatabase db = FirebaseDatabase.getInstance();
            DatabaseReference tokens = db.getReference("Tokens");
            Token token = new Token(refreshedToken, true);
            tokens.child(Common.currentUser.getPhone()).setValue(token);
        }

    }
}
