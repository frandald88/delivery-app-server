package com.example.lopezf.burrerosserver.Remote;

import com.example.lopezf.burrerosserver.Model.DataMessage;
import com.example.lopezf.burrerosserver.Model.MyResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by lopezf on 4/2/2018.
 */

public interface APIService {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAAR0aEb8w:APA91bG1zhf453sxyfqPNeXtdDMlXo1XcX412Cn1-HWMWfVCwoIQNBhOLuF-cONgzv1q1-C61gi7VfHa3EJakU0nHsbGFzGZ2eZdwd-vEPOu1YSx9CI3I0Nr_RiuRG264Wzzq_Alcwpl"


            }



    )
    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body DataMessage body);
}
